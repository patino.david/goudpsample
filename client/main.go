package main

import (
	"encoding/json"
	"log"
	"net"
	"udpServer/models"
)

func main() {
	udpServer, err := net.ResolveUDPAddr("udp", ":1053")
	if err != nil {
		log.Fatal("ResolveUDPAddr failed:", err.Error())
	}

	conn, err := net.DialUDP("udp", nil, udpServer)
	if err != nil {
		log.Fatal("Listen failed:", err.Error())
	}

	//close the connection when done
	defer func(conn *net.UDPConn) {
		_ = conn.Close()
	}(conn)

	// Make a json statement from the structure
	j, err := json.Marshal(models.Message{
		App:        "test",
		Multiplier: 1,
		User:       "patino.david",
		Function:   "upgrade_all_the_things",
	})
	if err != nil {
		log.Fatal("error marshalling payload:", err.Error())
	}

	// Send the payload
	_, err = conn.Write(j)
	if err != nil {
		log.Fatal("Write data failed:", err.Error())
	}

	// buffer to get data
	received := make([]byte, 1024)
	_, err = conn.Read(received)
	if err != nil {
		log.Fatal("Read data failed:", err.Error())
	}

	log.Fatal(string(received))
}
