package models

type Message struct {
	App        string  `json:"app"`
	Multiplier float32 `json:"multiplier"`
	User       string  `json:"user"`
	Function   string  `json:"function"`
}
