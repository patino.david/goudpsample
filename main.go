package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"udpServer/models"
)

func main() {
	// listen to incoming udp packets
	udpServer, err := net.ListenPacket("udp", ":1053")
	if err != nil {
		log.Fatal(err)
	}
	defer func(udpServer net.PacketConn) {
		_ = udpServer.Close()
	}(udpServer)

	for {
		buf := make([]byte, 1024)
		_, addr, err := udpServer.ReadFrom(buf)
		if err != nil {
			log.Println("error:", err.Error())
			continue
		}
		var message models.Message
		if err := json.Unmarshal(bytes.Trim(buf, "\x00"), &message); err != nil {
			log.Println("error unmarshalling payload:", err.Error())
			continue
		}
		go response(udpServer, addr, message)

	}
}

func response(udpServer net.PacketConn, addr net.Addr, buf models.Message) {
	j, err := json.Marshal(buf)
	if err != nil {
		log.Println("error marshalling message:", err.Error())
		return
	}

	responseStr := fmt.Sprintf("Received: %v!", string(j))

	_, _ = udpServer.WriteTo([]byte(responseStr), addr)
}
